```
     _/      _/        _/                                _/            _/    _/                
  _/_/_/_/  _/_/_/        _/_/_/      _/_/_/        _/_/_/    _/_/    _/  _/_/_/_/    _/_/_/  
   _/      _/    _/  _/  _/    _/  _/    _/      _/    _/  _/_/_/_/  _/    _/      _/    _/  
  _/      _/    _/  _/  _/    _/  _/    _/      _/    _/  _/        _/    _/      _/    _/  
   _/_/  _/    _/  _/  _/    _/    _/_/_/        _/_/_/    _/_/_/  _/      _/_/    _/_/_/  
                                      _/                                                  
                                  _/_/
```

License: GPL
Derivative: Kossel mini

This thing has a delta setup. It is a fork of Kossel mini (http://reprap.org/wiki/Kossel) and
indeed it still has some of the original 3D printed parts and is thus compatible with the
parts sold for Kossel mini.

What's different is:  
 - Adapted for Replicape + Manga Screen
 - Cable management    
 - Acrylic plates covering most of the open parts of the printer.  
 - CNC milled effector  
 - Mechanical Z-probe using the nozzle  

How to:
Step 1: Buy all the parts in the BOM.

Step 2: Print all the parts  
  - Fan holder.stl  
  - Effector-base.stl  
  - Motor-bottom-front.stl X2  
  - Motor-bottom-back.stl  
  - Screen-holder-column.stl X2
  - Replicape-holder.stl  

Step 3: Mill the effector in aluminium

Step 4: Laser cut the acrylic
